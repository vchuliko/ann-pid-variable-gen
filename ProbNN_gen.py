#require results of 4D parametrization (weights_biases.npy) and independent MC sample (B2JpsieeKstar_2016_MC.root)
import tensorflow as tf

import sys, os, root_numpy, numpy, random
sys.path.append("../")
from TensorFlowAnalysis import *
from ROOT import *
from rootpy.plotting import Hist, Hist2D
from root_numpy import array2root
#FOR TIME CONTROL
import time

SetSeed(1)

bounds_MC = [ (1.5, 5), (2.6, 4.8)]

# Ranges & names of variables
#For L1
#branches_MC = ["L1_eta", "L1_PT", "L1_MC15TuneV1_ProbNNe", "L1_PIDe", "L1_HasBremAdded", "L1_BremMultiplicity","L1_P"]
#branches_var_name = "L1_MC_ProbNNe"
#branches_file = "MC_modified_1.root"


#For L2
branches_MC = ["L2_eta", "L2_PT", "L2_MC15TuneV1_ProbNNe", "L2_PIDe", "L2_HasBremAdded", "L2_BremMultiplicity","L2_P"]
branches_var_name = "L2_MC_ProbNNe"
branches_file = "MC_modified_2.root"

# Definition of the phase space
bounds = [ (5.15, 5.6), (0., 10.), (1.5, 5.5), (2.6, 4.8)]
phsp = RectangularPhaseSpace( bounds )

#RESULT OF PARAMETRISATION
input_array = numpy.load("./trains/4D_Parametrisation_1001/4D_Parametrisation_best_score.npy", allow_pickle=True)

#BIASES & WEIGHTS FOR SIGNLA(BCG) PART
w_b_s = input_array[:-2] #weights + biases for sig, for background other two columns
(weights, biases) = InitWeightsBiases(w_b_s)

#FILL MC ARRAY
mc_array = root_numpy.root2array("B2JpsieeKstar_2016_MC.root", "tree", branches_MC)#643811
mc_array = root_numpy.rec2array(mc_array)
mc_array[:,0] = mc_array[:,0] #ETA
mc_array[:,1] = numpy.log10(mc_array[:,1]) #PT

#FILL DATA ARRAY

def model(x) : 
  return (MultilayerPerceptron(x, weights, biases) + 1e-20)

model_res = model(phsp.data_placeholder)

init = tf.global_variables_initializer()

#FILE & HIST FOR HIST SAVE
tree = TTree('test', 'test tree')

h1 = Hist(5, 0, 10, name = 'hist1')
tree.Branch('branch', h1, 'branch/D')

#CREATION ARRAY FOR RANDOM PROBNN GENERATED 
gen_array = numpy.array([])

#TIME START POINT
start_time = time.time()
sum_epoch_time = 0
time_show = 100 #number of epochs for showing the time

#SESSION
arr_size = len(mc_array)
print(len(mc_array))

with tf.Session() as sess:
  sess.run(init)
  for i in range(arr_size):#len(mc_array)
    current_time = time.time()-start_time
    one_epoch_time = current_time/float(i+1)#time for 1 epoch
    sum_epoch_time += one_epoch_time
    if i % time_show == 0:
      print("Time to finish: " + str( (arr_size - i)*sum_epoch_time/(float(i)+1)))
      print("Event #: " + str(i) + ":" + str(arr_size))
    probNN = 0
    eta = mc_array[i,0]
    pt = mc_array[i, 1]
    mb = 5.2791
    h1.Reset("ICESM")
    while probNN < 10:
        probNN += 2. #PROBNN STEP
        data_array = numpy.array([[mb, probNN, eta, pt]])
        y = sess.run(model_res, feed_dict = { phsp.data_placeholder : data_array})
        h1.fill(probNN, y[0])
    var = h1.GetRandom()
    gen_array = numpy.append(gen_array, float(var), axis = None)

h1.Draw("hist")

tree.Fill()

#SAVE GENERATED PROBNN
for i in range(len(branches_MC)):
  print(branches_MC[i])
  test_array = numpy.array(mc_array[:,i], dtype = [(branches_MC[i], numpy.float64)])
  if i == 0:
    array2root(test_array, branches_file, treename = 'tree', mode = 'recreate')
  else:
    array2root(test_array, branches_file, treename = 'tree', mode = 'update')

gen_array = numpy.array(gen_array, dtype = [(branches_var_name, numpy.float64)])
array2root(gen_array, branches_file, treename = 'tree', mode = 'update')
