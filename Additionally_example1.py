#mc_data.root -> Atlas-dataset (tt -> Jets): mc_117049.ttbar_had.root 21-Jul-2016 16:00 | 5,8Mb | 25170

import tensorflow as tf
import sys, os
sys.path.append("../../")
from TensorFlowAnalysis import *
import numpy
import root_numpy
import ROOT

#Range of variables 
bounds = [ (-200., 200.) ]

#Definition of the phase space
phsp = RectangularPhaseSpace( bounds )

SetSeed(1)

#Meta-parameters of the training procedure
n_hidden = [ 4, 3, 3, 3, 4]  # number of neurons in hidden layers 32 64
weight_penalty = 0.5      # penalty for L2 regularisation term
learning_rate = 0.005       # Learning rate for Adam optimiser 0 001
training_epochs = 1001    # Number of optimisation epochs
outfile = "1D_Parametrisation_"  # Prefix for the output files 
print_step = 10              # Print cost function every 50 epochs
display_step = 10            # Display fitted PDF and store to file every 200 epochs
threads = 8

n_input = len(bounds)   #number of input neurons
(weights_sig, biases_sig) = CreateWeightsBiases(n_input, n_hidden)

#create directory for trains results
path = r"./trains/" + outfile + str(training_epochs)
if not os.path.exists(path):
  os.makedirs(path)

data_array = root_numpy.root2array("mc_data.root", "mini", ["vxp_z"])
data_array = root_numpy.rec2array(data_array)
data_array[:,0] = data_array[:,0]

def model(x) : 
  m = phsp.Coordinate(x, 0)
  return (MultilayerPerceptron(x, weights_sig, biases_sig) + 1e-20)

model_data = model(phsp.data_placeholder)
model_norm = model(phsp.norm_placeholder)

nll = UnbinnedNLL(model_data, Integral(model_norm)) + L2Regularisation(weights_sig)*weight_penalty

# Create Adam optimiser
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(nll)

init = tf.global_variables_initializer()
session_conf = tf.ConfigProto( intra_op_parallelism_threads=threads, inter_op_parallelism_threads=threads)

with tf.Session(config = session_conf) as sess :
    sess.run(init)

    # Create normalisation sample + filter
    norm_sample = sess.run( phsp.UniformSample( 1000000 ) )
    #norm_sample = sess.run( phsp.RectangularGridSample( (400, 400) ) )
    array = sess.run(phsp.Filter(data_array))

    # Create multidimensional display
    if display_step != 0 : 
      display = MultidimDisplay(array, ["vxp_z"], phsp.Bounds(), bins1d=100,  pull = True)

    # Training cycle
    best_cost = 1e10
    for epoch in range(training_epochs):
        if display_step != 0 and (epoch % display_step == 0) : 

            fit_data = sess.run(model_norm, feed_dict = { phsp.norm_placeholder : norm_sample})
            display.draw(norm_sample, fit_data,  "./trains/" + outfile + str(training_epochs) + "/" + outfile + str(epoch) +".png")

        # Run optimization op (backprop) and cost op (to get loss value)
        _, c = sess.run([train_op, nll], feed_dict={phsp.data_placeholder: array, phsp.norm_placeholder: norm_sample })
      
        # Display logs per epoch step
        s = "Epoch %d, cost %.3f" % (epoch+1, c)
        print s

        if epoch % print_step == 0 :#save each ... epochs
            np.save("./trains/" + outfile +str(training_epochs) + "/" + outfile + "epoch_" + str(epoch+1), sess.run([weights_sig, biases_sig])) 
            if c < best_cost :      #save best nll
              best_cost = c
              np.save("./trains/" + outfile + str(training_epochs) + "/" + outfile + "best_score", sess.run([weights_sig, biases_sig]))