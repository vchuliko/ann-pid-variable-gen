from ROOT import *
import numpy
import root_numpy
from rootpy.plotting import Hist, Hist2D

def fill_hist(canv, hist_1, hist_2, place, data_array, cut_probNN, cut_nTracks, d_cut_eta, u_cut_eta, d_cut_pt, u_cut_pt):
  data_array_1 = data_array[:,0][((1-(1-data_array[:,0]**0.001)**0.1)> cut_probNN) & (data_array[:,1]>d_cut_eta) & (data_array[:,1]<u_cut_eta) & (data_array[:,2]>d_cut_pt) & (data_array[:,2]<u_cut_pt) & (data_array[:,3]> cut_nTracks)]
  data_array_2 = data_array[:,0][((1-(1-data_array[:,0]**0.001)**0.1)> cut_probNN) & (data_array[:,1]>d_cut_eta) & (data_array[:,1]<u_cut_eta) & (data_array[:,2]>d_cut_pt) & (data_array[:,2]<u_cut_pt) & (data_array[:,3]< cut_nTracks)]
  hist_1.fill_array((1-(1-data_array_1**0.001)**0.1))
  hist_1.Scale(1/hist_1.GetEntries())
  hist_1.markercolor = 'red'
  hist_1.markersize = 0.8
  hist_2.fill_array((1-(1-data_array_2**0.001)**0.1))
  hist_2.Scale(1/hist_2.GetEntries())
  hist_2.markercolor = 'navy'
  hist_2.markersize = 0.8
  hist_1.GetXaxis().SetTitle("ProbNN")
  hist_1.GetYaxis().SetTitle("Entries")
  print(hist_1.GetSumOfWeights())
  print(hist_2.GetSumOfWeights())
  return(canv.Update())

def value(x):
    array = root_numpy.root2array(name_input_file, name_tree, [x] )
    array = root_numpy.rec2array(array)
    MAX = numpy.amax(array)
    MIN = numpy.amin(array)
    MEAN = numpy.mean(array)
    print("Min value: " + str(MIN) + "\n" + "Max value: " + str(MAX) + "\n" + "Mean:" + str(MEAN) + "\n")

name_input_file="pidcalib_BJpsiEE_MD_TAGCUT.root"
name_tree = "DecayTree"

#Print list of branches

f = TFile.Open(name_input_file, "read")
tree = f.Get(name_tree)
for i in tree.GetListOfBranches():
        print(i.GetName())


var = raw_input("\nBranch name: ")

#Print variable range
value(var)

#Variable cut
var_cut = float(raw_input(var + " cut = "))

data_array = root_numpy.root2array(name_input_file, name_tree, ["probe_Brunel_MC15TuneV1_ProbNNe", "probe_ETA", "probe_PT", var])
data_array = root_numpy.rec2array(data_array)

c = TCanvas("canv1","",700,700)
c.Divide(2,2)
  
hist1 = Hist(100, 0.4, 1)
hist2 = Hist(100, 0.4, 1)
fill_hist(c, hist1, hist2, 1, data_array, 0.4, var_cut, 2, 3, 2500, 3500)
c.cd(1)
hist1.Draw()
c.cd(1)
hist2.Draw("same")

hist3 = Hist(100, 0.4, 1)
hist4 = Hist(100, 0.4, 1)
fill_hist(c, hist3, hist4, 1, data_array, 0.4, var_cut, 2, 3, 1300, 1700)
c.cd(2)
hist3.Draw()
c.cd(2)
hist4.Draw("same")

hist5 = Hist(100, 0.4, 1)
hist6 = Hist(100, 0.4, 1)
fill_hist(c, hist5, hist6, 1, data_array, 0.4, var_cut, 3, 4, 2500, 3500)
c.cd(3)
hist5.Draw()
c.cd(3)
hist6.Draw("same")

hist7 = Hist(100, 0.4, 1)
hist8 = Hist(100, 0.4, 1)
fill_hist(c, hist7, hist8, 1, data_array, 0.4, var_cut, 3, 4, 1300, 1700)
c.cd(4)
hist7.Draw()
c.cd(4)
hist8.Draw("same")