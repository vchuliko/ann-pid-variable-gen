#the same dataset as ex1

import tensorflow as tf
import sys, os, root_numpy, numpy
sys.path.append("../../")
from TensorFlowAnalysis import *
from ROOT import *
from rootpy.plotting import Hist, Hist2D

SetSeed(1)

#4D parameterization results
input_array = numpy.load("./trains/1D_Parametrisation_1001/1D_Parametrisation_best_score.npy", allow_pickle=True)

w_b_s = input_array[:] #weights + biases for sig

(weights, biases) = InitWeightsBiases(w_b_s)

data_array = root_numpy.root2array("mc_data.root", "mini", ["vxp_z"])
data_array = root_numpy.rec2array(data_array)
data_array[:,0] = data_array[:,0]

# Ranges of variables 
bounds = [ (-200., 200.)]
branches = ["vxp_z"]

# Definition of the phase space
phsp = RectangularPhaseSpace( bounds )

def model(x) : 
  m = phsp.Coordinate(x, 0)
  n = phsp.Coordinate(x, 1)
  return (MultilayerPerceptron(x, weights, biases) + 1e-20)

model_data = model(phsp.data_placeholder)
model_norm = model(phsp.norm_placeholder)

init = tf.global_variables_initializer()

with tf.Session() as sess :
    sess.run(init)

# Create normalisation sample + filter
    norm_sample = sess.run( phsp.UniformSample( 1000000 ) ) #200k
    array = sess.run(phsp.Filter(data_array))

#FOR ANY DIMENSIONS NUMBER

    display = MultidimDisplay(array, branches, phsp.Bounds(), bins1d=100, pull = True)

    fit_data = sess.run(model_norm, feed_dict = { phsp.norm_placeholder : norm_sample})
    display.draw(norm_sample, fit_data,  "./trains/1DMassFit_result.png")