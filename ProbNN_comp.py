#comparing the results of data-driven technique, MC and data. It requires independent data sample (B2JpsieeKstar_2016_Data.root) and two ntuples with generated ProbNN 
#for electron #1 (MC_modified_1.root) and electron #2 (MC_modified_2.root)
import root_numpy, numpy
from ROOT import *
from rootpy.plotting import Hist

#fill hist
def H_fill(arr1, w1, weights, bins_num):
  hist1 = Hist(bins_num, 0, 10)
  if weights == False: 
    hist1.fill_array(arr1)
  else: 
    hist1.fill_array(arr1, w1)
  return hist1

#merge e1 & e2 hists
def H_add(hist1, hist2, col):
  hist1.Add(hist2)
  hist1.GetXaxis().SetTitle("probNN")
  hist1.GetYaxis().SetTitle("events")
  hist1.SetLineColor(col)
  hist1.SetLineWidth(2)
  return hist1

#read ntuples
branches_MC_1 =  ["L1_MC_ProbNNe", "L1_MC15TuneV1_ProbNNe"] #E1
branches_MC_2 = ["L2_MC_ProbNNe", "L2_MC15TuneV1_ProbNNe"] #E2
branches_DATA = ["L1_MC15TuneV1_ProbNNe", "L2_MC15TuneV1_ProbNNe", "N_sig_sw"] #DATA

mc_array_1 = root_numpy.root2array("MC_modified_1.root", "tree", branches_MC_1) #FOR FIRST ELECTRON
mc_array_1 = root_numpy.rec2array(mc_array_1)
mc_array_1[:,0] = mc_array_1[:,0]
mc_array_1[:,1] = 10*((1-(1-mc_array_1[:,1]**0.05)**0.2))

mc_array_2 = root_numpy.root2array("MC_modified_2.root", "tree", branches_MC_2) #FOR SECOND ELECTRON
mc_array_2 = root_numpy.rec2array(mc_array_2)
mc_array_2[:,0] = mc_array_2[:,0]
mc_array_2[:,1] = 10*((1-(1-mc_array_2[:,1]**0.05)**0.2))

data_array = root_numpy.root2array("B2JpsieeKstar_2016_Data.root", "tree", branches_DATA) #FOR DATA
data_array = root_numpy.rec2array(data_array)
data_array[:,0] = 10*((1-(1-data_array[:,0]**0.05)**0.2)) #DATA E1
data_array[:,1] = 10*((1-(1-data_array[:,1]**0.05)**0.2)) #DATA E2
#data_array[:,2] column is weights.

#canv & hists
c = TCanvas("canv", "", 700, 700)
c.Divide(1,1)
c.cd(1)

#bins in hists
bins = 20 

#fill and merge hists
hist_param = H_add(H_fill(mc_array_1[:,0], 0, False, bins), H_fill(mc_array_2[:,0], 0, False, bins), 'red')
hist_param.Scale(1./hist_param.GetSumOfWeights())
hist_param.Draw("hist")

hist_mc = H_add(H_fill(mc_array_1[:,1], 0, False, bins), H_fill(mc_array_2[:,1], 0, False, bins), 'blue')
hist_mc.Scale(1./hist_mc.GetSumOfWeights())
hist_mc.Draw("hist same")

hist_data = H_add(H_fill(data_array[:,0], data_array[:,2], False, bins), H_fill(data_array[:,1], data_array[:,2], True, bins), 'green')
hist_data.Scale(1./hist_data.GetSumOfWeights())
hist_data.Draw("hist same")

#leg & upd
leg = TLegend(0.15 , 0.7, 0.45, 0.8)
leg.AddEntry(hist_param, "ProbNN from parameterization", "L")
leg.AddEntry(hist_mc, "mc", "L")
leg.AddEntry(hist_data, "data", "L")
leg.Draw()

c.Update()