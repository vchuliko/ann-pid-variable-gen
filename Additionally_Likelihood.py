import root_numpy, numpy
from ROOT import *
from rootpy.plotting import Hist

#directory with parameterization results and *.txt file contains likelihoods

input_array = numpy.loadtxt("./trains/4D_Parametrisation_1001/4D_Parametrisation_1001.txt")
print(input_array)

c = TCanvas("canv", "", 700, 700)
c.Divide(1,1)

hist1 = Hist(int(numpy.amax(input_array[:,0])), 0, numpy.amax(input_array[:,0]))
hist1.fill_array(input_array[:,0],input_array[:,1])
hist1.GetXaxis().SetTitle("epoch")
hist1.GetYaxis().SetTitle("-nll")
c.cd(1)
hist1.Draw("hist")