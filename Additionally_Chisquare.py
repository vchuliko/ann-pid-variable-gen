#require weights and biases in *.npy file (4D_Parametrisation_best_score.npy),
#calibration sample (pidcalib_BJpsiEE_MD_TAGCUT.root)
#1D fit results (1D_fit.npy)
import tensorflow as tf

import sys, os, root_numpy, numpy
sys.path.append("../")
from TensorFlowAnalysis import *
from ROOT import *
from rootpy.plotting import Hist, Hist2D

SetSeed(1)

# Ranges of variables 
bounds = [ (5.15, 5.6), (0., 10.), (1.5, 5.5), (2.6, 4.8)]
branches = ["B_M_DTF_Jpsi","probe_Brunel_MC15TuneV1_ProbNNe", "probe_ETA", "probe_PT"]


# Definition of the phase space
phsp = RectangularPhaseSpace( bounds )

#4D parameterization results
input_array = numpy.load("./trains/4D_Parametrisation_1001/4D_Parametrisation_best_score.npy")

w_b_s = input_array[:-2] #weights + biases for sig 
w_b_b = input_array[-2:] #weigths + biases for bcg

(weights, biases) = InitWeightsBiases(w_b_s)
(weights_, biases_) = InitWeightsBiases(w_b_b)

data_array = root_numpy.root2array("pidcalib_BJpsiEE_MD_TAGCUT.root", "DecayTree", branches)
data_array = root_numpy.rec2array(data_array)
data_array[:,0] = data_array[:,0]/1000.
data_array[:,1] = 10*((1-(1-data_array[:,1]**0.05)**0.2))
data_array[:,2] = data_array[:,2]
data_array[:,3] = numpy.log10(data_array[:,3])

#1D Fit results
input_array = numpy.load("./trains/1D_fit.npy")
gaus_sigma, gaus_mean, gaus_weight, cauchy_mean, cauchy_gamma, weight, param_0, param_1 = [input_array[i] for i in range(len(input_array))]

def cauchy(weight, x, x_mean, gamma): return weight/(Pi()*gamma + Pi()*(x-x_mean)**2/gamma)

def gaus(weight, x_mean, x, sigma): return weight*Exp((-0.5)*(x-x_mean)**2/(sigma**2))/Sqrt(sigma**2*2*Pi())

def model(x) : 
  m = phsp.Coordinate(x, 0)
  n = phsp.Coordinate(x, 1)
  return (MultilayerPerceptron(x, weights, biases) + 1e-20)*(gaus(gaus_weight, gaus_mean, m , gaus_sigma) + cauchy(1-gaus_weight, m, cauchy_mean, cauchy_gamma)) + (MultilayerPerceptron(x, weights_, biases_) + 1e-20)*Exp(-m*param_0+param_1)*weight

model_data = model(phsp.data_placeholder)
model_norm = model(phsp.norm_placeholder)

init = tf.global_variables_initializer()

with tf.Session() as sess :
    sess.run(init)

# Create normalisation sample + filter
    norm_sample = sess.run( phsp.UniformSample( 500000 ) ) #200k
    array = sess.run(phsp.Filter(data_array))

#FOR ANY DIMENSIONS NUMBER
    a, dim = array.shape #dim - number of dimensions
    bins_num = 4 # bins number
    print("dim = ", dim)
    chi2_columns = bins_num**dim

    display = MultidimDisplay(array, branches, phsp.Bounds(), bins1d=40, bins2d = bins_num, pull = True)

    fit_data = sess.run(model_norm, feed_dict = { phsp.norm_placeholder : norm_sample})
    display.draw(norm_sample, fit_data,  "./trains/4DMassFit_result.png")


#CHI2
    H1, norm_mass = numpy.histogramdd(norm_sample, bins = bins_num, weights = fit_data)
    H2, arr_mass = numpy.histogramdd(array, bins = bins_num)

    print(H1*numpy.sum(H2)/numpy.sum(H1))
    print(H2)

    H1 = (H1*numpy.sum(H2)/numpy.sum(H1)).reshape(chi2_columns,1) #norm
    H2 = H2.reshape(chi2_columns,1)#data

    chi2 = numpy.empty([chi2_columns,2])

    c = TCanvas("canv", "", 700, 700)
    c.Divide(1,2)
    hist1 = Hist(chi2_columns, 0, chi2_columns)
    hist2 = Hist(chi2_columns, 0, chi2_columns) #norm
    hist3 = Hist(chi2_columns, 0, chi2_columns)#data
    
    chi2_max = 0
    for i in range(len(H1)):
      chi2[i, 0] = (H1[i]-H2[i])**2/H1[i]
      if(chi2_max < chi2[i, 0]): chi2_max, bin_ = chi2[i, 0], i
      chi2[i, 1] = i
      print(chi2[i, 0])
      print(chi2[i, 1])
      hist1.fill(chi2[i,1], chi2[i, 0])
      hist2.fill(chi2[i,1], H1[i]) #norm
      hist3.fill(chi2[i,1], H2[i]) #data
    hist1.GetXaxis().SetTitle("bin")
    hist1.GetYaxis().SetTitle("\chi^2")
    c.cd(1)
    hist1.Draw("hist")
    c.cd(2)
    hist2.SetLineColor('red')
    hist3.SetLineColor('blue')
    hist2.Draw("hist")
    hist3.Draw("hist same")
    c.Update()

print("\n"+ "chi2max(in bins) = "+ str(chi2_max))
print("bin:" +str(bin_))
print("norm:" +str(H1[bin_]) + " data:"+ str(H2[bin_]))
print("chi2 = " + str(numpy.sum(chi2[:,0])))