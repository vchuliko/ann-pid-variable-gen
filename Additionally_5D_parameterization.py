#The same as 4D
import tensorflow as tf

import sys, os
sys.path.append("../")
from TensorFlowAnalysis import *
import numpy
import root_numpy
import ROOT
import math
import time
from rootpy.plotting import Hist

  # Ranges of variables 
bounds = [ (5.15, 5.6), (0., 10.), (1.8, 5.5) , (2.6, 4.8), (0.5, 3.)]

  # Definition of the phase space
phsp = RectangularPhaseSpace( bounds )

SetSeed(1)

# Meta-parameters of the training procedure
n_hidden = [ 64, 32, 16]  # number of neurons in hidden layers 32 64
weight_penalty = 15.0      # penalty for L2 regularisation term
weight_penalty_ = 0.01      # penalty for L2 bcg
learning_rate = 0.1       # Learning rate for Adam optimiser 0 001
training_epochs = 1001    # Number of optimisation epochs
outfile = "5D_Parametrisation_"  # Prefix for the output files 
print_step = 50              # Print cost function every 50 epochs
display_step = 50             # Display fitted PDF and store to file every 200 epochs
threads = 8

n_input = len(bounds)   # number of input neurons
(weights_sig, biases_sig) = CreateWeightsBiases(n_input, n_hidden)
(weights_bcg, biases_bcg) = CreateWeightsBiases(n_input, n_hidden)

path = r"./trains/" + outfile + "_" +str(training_epochs) #creating directory for output files 
if not os.path.exists(path):
  os.makedirs(path)

data_array = root_numpy.root2array("pidcalib_BJpsiEE_MD_TAGCUT.root", "DecayTree", ["B_M_DTF_Jpsi","probe_Brunel_MC15TuneV1_ProbNNe", "probe_ETA", "probe_PT", "nTracks"])
data_array = root_numpy.rec2array(data_array)
data_array[:,0] = data_array[:,0]/1000.
data_array[:,1] = 10*((1-(1-data_array[:,1]**0.05)**0.2))
data_array[:,2] = data_array[:,2]
data_array[:,3] = numpy.log10(data_array[:,3])
data_array[:,4] = numpy.log10(data_array[:,4])

#read values of variables from 1D-fit
input_array = numpy.load("./trains/1D_fit.npy")
gaus_sigma, gaus_mean, gaus_weight, cauchy_mean, cauchy_gamma, weight, param_0, param_1 = [input_array[i] for i in range(len(input_array))]


def cauchy(weight, x, x_mean, gamma): return weight/(Pi()*gamma + Pi()*(x-x_mean)**2/gamma)

def gaus(weight, x_mean, x, sigma): return weight*Exp((-0.5)*(x-x_mean)**2/(sigma**2))/Sqrt(sigma**2*2*Pi())

def model(x) : 
  m = phsp.Coordinate(x, 0)
  return (MultilayerPerceptron(x, weights_sig, biases_sig) + 1e-20)*(gaus(gaus_weight, gaus_mean, m , gaus_sigma) + cauchy(1-gaus_weight, m, cauchy_mean, cauchy_gamma)) + (MultilayerPerceptron(x, weights_bcg, biases_bcg) + 1e-20)*Exp(-m*param_0+param_1)*weight

model_data = model(phsp.data_placeholder)
model_norm = model(phsp.norm_placeholder)

nll = UnbinnedNLL(model_data, Integral(model_norm)) + L2Regularisation(weights_sig)*weight_penalty + L2Regularisation(weights_bcg)*weight_penalty_

# Create Adam optimiser
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
train_op = optimizer.minimize(nll)

init = tf.global_variables_initializer()
session_conf = tf.ConfigProto( intra_op_parallelism_threads=threads, inter_op_parallelism_threads=threads)

#time
sum_time = 0

#write likelihood values
f = open("./trains/" + outfile + "_" +str (training_epochs) + "/" + outfile + str(training_epochs) + ".txt", "w")

with tf.Session(config = session_conf) as sess :
    sess.run(init)

    # Create normalisation sample + filter
    norm_sample = sess.run( phsp.UniformSample( 1000000 ) ) #200k
    array = sess.run(phsp.Filter(data_array))

    # Create multidimensional display
    if display_step != 0 : 
      display = MultidimDisplay(array, ["M_B", "ProbNNe", "probe_ETA" ,"probe_PT", "nTracks"], phsp.Bounds(), bins1d=40, bins2d = 20, pull = True)

    # Training cycle
    best_cost = 1e10
    for epoch in range(training_epochs):
        epoch_time = time.time()
        if display_step != 0 and (epoch % display_step == 0) : 

            fit_data = sess.run(model_norm, feed_dict = { phsp.norm_placeholder : norm_sample})
            display.draw(norm_sample, fit_data,  "./trains/" + outfile + "_" +str(training_epochs) + "/" + outfile + str(epoch) +".png")

        # Run optimization op (backprop) and cost op (to get loss value)
        _, c = sess.run([train_op, nll], feed_dict={phsp.data_placeholder: array, phsp.norm_placeholder: norm_sample })
      
        # Display logs per epoch step
        s = "Epoch %d, cost %.9f" % (epoch+1, c)
        print s
        f.write(str(epoch+1) + " " + str(c) + "\n")
        if epoch % print_step == 0 :#save each ... epochs
            np.save("./trains/" + outfile + "_" +str(training_epochs) + "/" + outfile + "_epoch_" + str(epoch+1), sess.run([weights_sig, biases_sig, weights_bcg, biases_bcg])) 
            if c < best_cost :#save best nll
              best_cost = c
              np.save("./trains/" + outfile + "_" +str(training_epochs) + "/" + outfile + "_best_score", sess.run([weights_sig, biases_sig, weights_bcg, biases_bcg]))
        sum_time += float(time.time() - epoch_time)
        print("Average for 1 epoch = " + str(sum_time/(epoch+1)))
        print("Seconds for this epoch = " + str(time.time()-epoch_time))
        print("Seconds to finish = " + str((training_epochs - epoch)*sum_time/(epoch+1)))
f.close()