#Code requires data sample for instance : pidcalib_BJpsiEE_MD_TAGCUT.root

import tensorflow as tf
import sys, os, root_numpy, ROOT

sys.path.append("../TensorFlowAnalysis") #path to TFA libs
path_to_ds  = "./datasets/"
ds_name     = "pidcalib_BJpsiEE_MD_TAGCUT.root"
tree_name   = "DecayTree"
leaf_name   = "B_M_DTF_Jpsi"

from TensorFlowAnalysis import *

#Ranges of variables 
bounds = [(5.15, 5.6)]

#Definition of the phase space
phsp = RectangularPhaseSpace( bounds )

#read data from root-file
array = root_numpy.root2array(path_to_ds + ds_name, tree_name, [leaf_name] )
array = root_numpy.rec2array(array)
array = array/1000.

#set parameters for signal:
gaus_sigma  = FitParameter("gaus_sigma", 0.01, 0.005, 0.015, 1e-6)
gaus_mean   = FitParameter("gaus_mean", 0., 5.27, 5.3, 1e-6)
gaus_weight = FitParameter("gaus_weight", 0., 0.0, 1.,1e-6)

cauchy_mean  = FitParameter("cauchy_mean", 0., 5.2, 5.4, 1e-6)
cauchy_gamma = FitParameter("cauchy_gamma", 0.001, 0.01, 1.,1e-6)

#set parameters for background:
weight  = FitParameter("weight", 0.5, 0.,1.,1e-6)
param_0 = FitParameter("param_0", 3.0, 1., 3.0,1e-6)
param_1 = FitParameter("param_1", 21., 21., 22., 1e-6)

#create functions for MINUIT's fit
def cauchy(weight, x, x_mean, gamma): return weight/(Pi()*gamma + Pi()*(x-x_mean)**2/gamma)

def gaus(weight, x_mean, x, sigma): return weight*Exp((-0.5)*(x-x_mean)**2/(sigma**2))/Sqrt(sigma**2*2*Pi())

#create model function
def model(x) : 
  m = phsp.Coordinate(x, 0)
  return (gaus(gaus_weight, gaus_mean, m , gaus_sigma) + cauchy(1-gaus_weight, m, cauchy_mean, cauchy_gamma) + Exp(-m*param_0+param_1)*weight)

#create models for data and normalization samples
model_data = model(phsp.data_placeholder)
model_norm = model(phsp.norm_placeholder)

nll = UnbinnedNLL(model_data, Integral(model_norm))
init = tf.global_variables_initializer()

#create tensorflow session and do variables initialization
sess = tf.Session()
sess.run(init)

norm_sample = sess.run( phsp.RectangularGridSample( (10000,) ) ) 

#run session
array = sess.run(phsp.Filter(array))

result = RunMinuit(sess, nll, { phsp.data_placeholder : array, phsp.norm_placeholder : norm_sample} )
fit_data = sess.run(model_norm, feed_dict = { phsp.norm_placeholder : norm_sample})

display = MultidimDisplay(array, ["Invariant mass, GeV"], phsp.Bounds(), bins1d=250, bins2d = 250, pull = True)
display.draw(norm_sample, fit_data,  "./trains/MassFit.png")

#creating directory for fit parameters
path = r"./trains/"  
if not os.path.exists(path):
  os.makedirs(path)

#save values of the fit variables
np.save("./trains/" + "1D_fit", sess.run([gaus_sigma, gaus_mean, gaus_weight, cauchy_mean, cauchy_gamma, weight, param_0, param_1]))