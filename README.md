Requirements:

Lib | Version
---|---
python | 2.7.15+
ROOT | 6.16/00
tensorflow | 1.4.0
root-numpy | 4.8.0
numpy      | 1.16.5


```bash
sudo pip install tensorflow==1.4.0
sudo ROOTSYS=$ROOTSYS pip install root_numpy  
pip install rootpy

git clone https://gitlab.cern.ch/poluekt/TensorFlowAnalysis
```

[**1. 1D_massfit.py**](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/1D_massfit.py)

Fitting the B-mass distribution in MINUIT, separating signal and background 
events. Signal is fitted used the Gaussian and Cauchy functions and Background 
used Exponent. Output file is numpy array that contains fit parameters: mean, 
sigma, weight for Gaussian, mean and gamma for Cauchy, and lambda1, lambda2, 
weight for Exponent.

[**2. 4D_parametrisation.py**](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/4D_parametrisation.py)

Multivariate parametrisation.

[**3. ProbNN_gen.py**](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/ProbNN_gen.py)

ProbNNe generation based on the obtained parameterization results.

[**4. ProbNN_comp.py**](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/ProbNN_comp.py)

Comparing results from different techniques.

***Additionally**

[I) Chisquare](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/Additionally_Chisquare.py)

[II) Likelihood values](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/Additionally_Likelihood.py)

[III) Correlation](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/Additionally_Correlation.py)

[IV) 5D parameterization](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/Additionally_5D_parameterization.py)

[V) Example 1: 1D parameterization](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/Additionaly_example1.py)

[VI) Example 2: Read weights and biases from 1D parameterization output files](https://gitlab.cern.ch/vchuliko/ann-pid-variable-gen/blob/master/Additionaly_example2.py)